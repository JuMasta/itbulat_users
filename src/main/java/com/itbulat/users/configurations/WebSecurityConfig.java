package com.itbulat.users.configurations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.ExceptionTranslationFilter;
import com.itbulat.users.filters.AuthFilter;

@EnableWebSecurity
public class WebSecurityConfig {

	Logger logger = LoggerFactory.getLogger(WebSecurityConfig.class);

	@Autowired
	AuthFilter authFilter;

	@Bean
	@Order(1)
	public SecurityFilterChain createUserChain(HttpSecurity http) throws Exception {

		http.requestMatchers((request) -> {
			request.antMatchers(HttpMethod.POST, "/user/");
		}).authorizeRequests().antMatchers(HttpMethod.POST, "/user/")
		.permitAll()
		.and()
		.csrf()
		.disable()
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

		return http.build();
	}

	@Bean
	@Order(2)
	public SecurityFilterChain getResourcesFilterChain(HttpSecurity http) throws Exception {
		http.requestMatchers((request) -> {
			request.antMatchers(HttpMethod.DELETE, "/user/").regexMatchers(HttpMethod.GET, "/user/\\d*");
		}).authorizeRequests().anyRequest().authenticated().and().csrf().disable().sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				.and()
				.anonymous().disable().addFilterBefore(authFilter, ExceptionTranslationFilter.class);
		return http.build();
	}

}