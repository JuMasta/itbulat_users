package com.itbulat.users.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity
@Table(name="users",
uniqueConstraints=
@UniqueConstraint(columnNames={"phoneNumber"})
		)
public class User {
	
	
	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
     private Long id;
    
	
    private String phoneNumber;
    
    private String password;
    
    private String name;
    
	private String secondName;
    
    private byte userType; 
    


	@Override
	public String toString() {
		return "User [id=" + id + ", phoneNumber=" + phoneNumber + ", password=" + password + ", name=" + name
				+ ", secondName=" + secondName + ", userType=" + userType + "]";
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public Long getId() {
		return id;
	}

	public void setPhoneNumber(String phone_number) {
		this.phoneNumber = phone_number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public byte getUserType() {
		return userType;
	}

	public void setUserType(byte userType) {
		this.userType = userType;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	

}
