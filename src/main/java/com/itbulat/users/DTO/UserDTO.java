package com.itbulat.users.DTO;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class UserDTO {
	
	@NotEmpty(message = "phoneNumber can't be empty")
	@Pattern(regexp= "\\+7\\(\\d{3}\\)\\d{3}-\\d{2}-\\d{2}", message="Not correct phoneNumber")
    private String phoneNumber;
    
	@NotEmpty
    private String password;
    
	@NotEmpty
    private String name;
    
	@NotEmpty
	private String secondName;
    
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public byte getUserType() {
		return userType;
	}

	public void setUserType(byte userType) {
		this.userType = userType;
	}

	@NotNull
    private byte userType;
	
	
}
