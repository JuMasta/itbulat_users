package com.itbulat.users.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.HashSet;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import com.itbulat.users.DTO.UserDTO;
import com.itbulat.users.models.User;
import com.itbulat.users.repositories.UserRepository;

@Service
public class UserService {
	
	Logger logger = LoggerFactory.getLogger(UserService.class);
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
    @Autowired
    private UserRepository userRepository;

    public User register(UserDTO userDTO)
    {
    	User user = new User();
    	BeanUtils.copyProperties(userDTO, user);
    	user.setPassword(passwordEncoder.encode(user.getPassword()));
    	logger.info("info");
    	return userRepository.save(user);
    }
    
    public User getById(long userId)
    {
		return userRepository.findByid(userId);
    }

    public User save(User user)
    {
    	return userRepository.save(user);
    }
	
	public User getByPhoneNumber(String phoneNumber)  {
		return userRepository.findByPhoneNumber(phoneNumber);
	}
	
	public List<User> getAll()
	{
		return userRepository.findAll();
	}
	
	public void deleteById(long userId)
	{
		userRepository.deleteById(userId);
	}
	
	public void delete(User user)
	{
		userRepository.delete(user);
	}

	public UserDetails loadUserByPhoneNumber(String phoneNumber) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		
		User user = userRepository.findByPhoneNumber(phoneNumber);
		if (user == null)
			throw new UsernameNotFoundException("User not found");
				
		return new org.springframework.security.core.userdetails.User(user.getPhoneNumber(), user.getPassword(), new HashSet<>() );
	}
	
    
}
