package com.itbulat.users.controllers;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itbulat.users.DTO.UserDTO;
import com.itbulat.users.models.User;
import com.itbulat.users.services.UserService;

@RestController
@RequestMapping("user")
public class UsersController {
	
	Logger logger = LoggerFactory.getLogger(UsersController.class);
	
	@Autowired
	UserService userService;
	
	@GetMapping
	public ResponseEntity<List<User>> getUsers()
	{
		return new ResponseEntity<List<User>>(userService.getAll(), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<User> getUser(@PathVariable long id)
	{
		User user = userService.getById(id);
		return new ResponseEntity<>(user, HttpStatus.OK);	
	}
	
	@PostMapping("/")
	public ResponseEntity<User> createUser(@RequestBody @Valid UserDTO user)
	{
		
		return new ResponseEntity<>(userService.register(user), HttpStatus.OK);	
	}
	
	
	@PutMapping("/")
	public ResponseEntity<User> updateUser(Authentication authentication)
	{
		
		String phoneNumber = authentication.getName();		
		return new ResponseEntity<>(userService.save(userService.getByPhoneNumber(phoneNumber)), HttpStatus.OK);			
	}
	
	
	@DeleteMapping("/")
	public ResponseEntity<String> deleteUser(Authentication authentication)
	{
		String phoneNumber = authentication.getName();		
		userService.delete(userService.getByPhoneNumber(phoneNumber));
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	

}
